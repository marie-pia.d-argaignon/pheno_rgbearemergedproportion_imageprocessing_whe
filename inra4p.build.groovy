def build() {

    stage('Parsing version') {
        VERSION = sh(
			script: "git describe",
			returnStdout: true
			).trim()
        echo("Version=${VERSION}")
    }

    stage('Docker build') {
        sh('echo "Docker build"')
        sh("docker build -t pheno_rgbearemergedproportion_imageprocessing_whe:${VERSION} -f dockerfile .")
    }

}

return this
