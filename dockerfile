FROM python:3.8-slim

MAINTAINER Eric David - Ephesia (eric.david@ephesia-consult.com)

ENV PYTHONPATH /app
ENV PYTHONUNBUFFERED=1

WORKDIR /app

COPY requirements.txt /app
RUN python -m pip install --upgrade pip
RUN pip --no-cache-dir install -r requirements.txt

COPY models /app/models
COPY module /app/module
COPY main.py /app
COPY data /app/data


ENTRYPOINT ["python", "/app/main.py"]
