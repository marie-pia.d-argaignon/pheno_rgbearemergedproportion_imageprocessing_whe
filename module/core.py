# -*- coding: utf-8 -*-
"""
Created on Fri Jan  6 08:44:27 2023

@author: mpdargaignon
"""
import numpy as np
import json
from tqdm import tqdm
import sys
import cv2
import matplotlib.pyplot as plt
import os
from pathlib import Path
from datetime import datetime, timezone
from PIL import Image
import skimage.transform as st
import logging
import keras
# from keras.models import load_model
import keras.backend as K
import tensorflow
from tensorflow.keras.models import load_model
from tensorflow.keras.applications import ResNet50
from keras.layers import Flatten, Dense, Dropout, GlobalAveragePooling2D
from keras.models import Model
########################################################################################################################
# Data processing
logger = logging.getLogger('__main__')
IMG_WIDTH, IMG_HEIGHT, IMG_CHANNELS = 256, 256, 3

def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

def run(image_paths, rgb_metadata, weights, output_folder):
    """Run My Module process"""
    time = datetime.now(timezone.utc)
    processing_date = '{:02d}/{:02d}/{:02d}'.format(time.day, time.month, time.year)
    logging.debug('processing_date: '+ str(processing_date))
    #get plot_name
    plot_name = (os.path.split(rgb_metadata)[1]).split('_')[1] + '_' + (os.path.split(rgb_metadata)[1]).split('_')[2] 
    logging.debug('plot_name: '+ str(plot_name))
    #create density_csv
    patchs_csv = Path(output_folder + '/' + plot_name + '_emerged_ear_proportion.csv')
    basic_header = [u'PlotId',
                     u'AcquisitionDate',
                     u'EmergedEarProportion(%)'
                     ]
    proportion_file = open(patchs_csv, 'wt')
    header = ";".join(basic_header) + "\n"
    proportion_file.write(header)
    proportion_file.flush()
    loaded_model = load_model(weights, custom_objects={'f1_m':f1_m})
   
    
    ########
    #lecture metadata file
    ########
    rgb_metadata = json.load(rgb_metadata.open())
    AcquisitionDate = rgb_metadata["session"]["date"]
    camera_parameters = {}
    camera_45 = []
    #paramètres camera
    for sensor in rgb_metadata["sensors"]:
        camera_parameters[sensor["description"]] = {}
        if abs(int(sensor["position"]["roll"])) < 175:
            camera_45.append(sensor["description"][-1])
    plot_nb = rgb_metadata["plot"]["id"]
    patchs_proportion = 0
    nb_image = 0
    nb_patchs = 0
    nb_patchs_ear = 0
    #start processing density on each nadir image
    for image in tqdm(image_paths):
        image_name = os.path.split(image)[1]
        if (str(image_name.split('_')[1] + '_' + image_name.split('_')[2])) == str(plot_nb) :
            camera_id = image_name.split('camera_')[1][0]
            if camera_id in camera_45:
                nb_image = nb_image + 1
                logging.debug('process '+ str(image_name))
                A = Image.open(image)
                shape = A.size
                
                # A = A.crop((126, 126, 1918, 1918))
                A = np.array(A)
                A = cv2.cvtColor(A, cv2.COLOR_RGB2BGR)
                A= st.rescale(A,0.5,multichannel = True, anti_aliasing=True, preserve_range=True)
                X = []
                col_range = range(0, A.shape[1]-128, int(IMG_HEIGHT/2))
                row_range = range(0, A.shape[0]-128, int(IMG_WIDTH/2))
                for c in range(0, len(col_range)-2):
                    for r in range(0, len(row_range)-2):
                        sub_img = A[row_range[r]:row_range[r+2], col_range[c]:col_range[c+2]]
                        # print((row_range[r],row_range[r+2]),( col_range[c],col_range[c+2]))
                        X.append(sub_img)
                       
                X = np.asarray(X, dtype='uint8')

                # predict using the model
                pred = loaded_model.predict(X)
                class_det = np.round(pred)
                # store the date and number of subsets with ears
                
                nb_patchs_ear += np.sum(class_det)
                nb_patchs += len(class_det)
            else:
                continue
        else:
            continue
        
        patchs_proportion = nb_patchs_ear / nb_patchs * 100
        
    
    if nb_image == 0 :
        logger.error('no valid image available for ear detection (only nadir, require 45°)')
        patchs_proportion = 'no valid image available for density evaluation (only nadir, require 45°)'
    res = [plot_name, AcquisitionDate,
                     str(patchs_proportion),
                     ]
    res = ";".join(res) + "\n"
    proportion_file.write(res)
    return
    

    