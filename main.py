# -*- coding: utf-8 -*-
"""
Created on Fri Jan  6 08:39:52 2023

@author: mpdargaignon
"""

import argparse
import logging
import os
import time
import sys

from module import core


from module import error
from pathlib import Path
####
PROCESS_NAME = "Wheat head patch classification"
#########################################

def extant_file(x):
    """
    'Type' for argparse - checks that file exists but does not open.
    """
    if not os.path.exists(x):
        # Argparse uses the ArgumentTypeError to give a rejection message like:
        # error: argument input: x does not exist
        raise argparse.ArgumentTypeError("{0} does not exist".format(x))
    return Path(x)
    

def create_parser():
    """Create Command Line Interface arguments parser
     """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "image_paths",
        nargs = '+',
        help = 'paths to each image of the plot',
        type = extant_file)
    
    parser.add_argument(
        "--rgb_metadata_path",
        help = 'path to rgb_metadata, output of rgb extraction',
        required = True,
        type = extant_file)
    
    parser.add_argument(
        "--model_path",
        required = True,
        help = 'paths to the model for ear presence classification',
        type = extant_file)
    
    parser.add_argument(
        "--output_folder",
        help="path to the output folder",
        required = True,
        type = str)

    
    parser.add_argument(
        "-v",
        "--verbose",
        help="activate output verbosity",
        action="store_true",
        default=False)

    return parser.parse_args()


###############################################################################
#PROCESS
###############################################################################
if __name__ == '__main__':
    timer_start = time.time()
    try:
        # Manage CLI arguments
        args = create_parser()

        # Init Logger
        logger = logging.getLogger()
        if args.verbose:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.addHandler(logging.FileHandler(os.path.splitext(__file__)[0] + '.log', 'w'))
        if str(args.rgb_metadata_path)[-4:] != 'json':
            raise error.DataError(args.rgb_metadata_path)
            
        logging.debug("CLI arguments are valid")
        #weights = utils.findfile('best.pt', os.path.dirname(os.path.abspath(__file__)))
        
        #if not os.path.exists(weights):
        #    raise error.DataError(weights)
        # Creating output folder if doesn't exist
        if not os.path.exists(args.output_folder):
            logging.debug("Create output folder '" + args.output_folder + "'")
            os.mkdir(args.output_folder)
            # Run module core process
        
        core.run(
            image_paths = args.image_paths,
            rgb_metadata = args.rgb_metadata_path,
            weights = args.model_path,
            output_folder = args.output_folder)
    
        logging.info("Process '" + PROCESS_NAME + "' complete")
    
    except error.DataError as e:
        # When DataError is raised, the process fails without killing the overall execution
        logging.exception(e)
        logging.error("Process '" + PROCESS_NAME + "' failed")
    except Exception as e:
        # Every other errors kill the overall execution
        logging.exception(e)
        logging.error("Process '" + PROCESS_NAME + "' failed")
        sys.exit(1)
    finally:
        timer_end = time.time()
        logging.debug("Process '" + PROCESS_NAME + "' took " + str(int(timer_end - timer_start)) + " seconds")


